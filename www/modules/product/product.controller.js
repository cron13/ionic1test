angular
  .module('ProductModule', [])
  .controller('ProductController', function (
    $scope,
    $stateParams,
    CatalogService
  ) {
    $scope.product = CatalogService.get(parseInt($stateParams.id));
    console.log($stateParams);
    console.log($scope);
  });
