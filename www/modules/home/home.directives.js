angular.module('HomeDirectivesModule', []).directive('appCard', function () {
  return {
    scope: {
      card: '<',
      test: '<',
      onTitleChanged: '&',
    },
    templateUrl: 'modules/home/templates/card.template.html',
    controller: function ($scope, $element) {
      $scope.changeTitle = function () {
        $scope.card.title = 'title changed';
        $scope.test = 'test changed';
        //$scope.onTitleChanged({ payload: $scope.card.title, test: 'passed' });
      };
    },
  };
});
