angular
  .module('HomeModule', [])
  .controller('HomeController', function (
    $scope,
    CatalogService,
    $ionicModal,
    $ionicPopover
  ) {
    $scope.title = 'Домашняя страница';
    $scope.cards = CatalogService.show();

    $ionicModal
      .fromTemplateUrl('modules/home/templates/modal.template.html', {
        scope: $scope,
        animation: 'slide-in-up',
      })
      .then(function (modal) {
        $scope.modal = modal;
      });

    $scope.openModal = function () {
      $scope.modal.show();
    };
    $scope.closeModal = function () {
      $scope.modal.hide();
    };

    $ionicPopover
      .fromTemplateUrl('modules/home/templates/popover.template.html', {
        scope: $scope,
      })
      .then(function (popover) {
        $scope.popover = popover;
      });

    $scope.openPopover = function ($event) {
      $scope.popover.show($event);
    };

    $scope.cardTitleChanged = function (payload, test) {
      $scope.title = test;
      console.log(payload, test);
    };
  });
