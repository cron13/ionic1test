angular.module('CatalogModule', []).factory('CatalogService', function () {
  this.items = [
    {
      id: 1,
      title: 'Велик',
    },
    {
      id: 2,
      title: 'Палатка',
    },
    {
      id: 3,
      title: 'Лыжи',
    },
    {
      id: 4,
      title: 'Спальник',
    },
    {
      id: 5,
      title: 'Самокат',
    },
    {
      id: 6,
      title: 'Мангал',
    },
  ];

  this.show = function () {
    return this.items;
  };

  this.get = function (id) {
    return this.items.find(function (item) {
      return item.id === id;
    });
  };

  return this;
});
